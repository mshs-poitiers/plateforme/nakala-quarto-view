# Nakala Quarto View
*David Chesnet, Michael Nauge. Univ. Poitiers*

Preuve de concept (POC) de l'utilisation du système de publication [Quarto](https://quarto.org/) pour la génération de site web "*bibliothèque numérique*" pour la navigation et la visualisation de données déposées sur l'entrepôt de données [Nakala](https://nakala.fr/).

- Consulter [un site web de démo](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo) généré par ce POC deployé via [gitlab Pages](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/quarto-bib-num-demo) ou un vrai site [Archives Eduardo Manet (sur ce Gitlab)](https://mshs-poitiers.gitpages.huma-num.fr/crla/eduardo-manet/)

- Exemple de visualisation d'un [lot d'images](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/dataPost/10_34847-nkl_ffabdg38/10_34847-nkl_25f31n26.html) utilisant le end-point IIIF

- Exemple de [spatialisation](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/collectionPost/10_34847-nkl_ffabdg38.html) via leaflet utilisant le dcterms:spatial.

- Exemple de [visualisation de statistiques](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/quarto-bib-num-demo/collectionPost/stats/10_34847-nkl_ffabdg38.stats.html) via plotlyExpress.

Le fichier [NakalaQuartoView.pdf](NakalaQuartoView.pdf) contient une présentation de la mise en oeuvre de l'outil.

Le dossier *code_python* contient une version exécutable du programme python.


## Besoins

Créer des sites webs personnalisables de consultation de données déposées sur l'entrepôt Nakala, tout en minimisant leurs coûts de fabrication et de maintenance.

## Solution proposée

Utiliser un générateur de site web statique avec un déploiement des fichiers générés html (et javascript, css) sur une gitlab Page.

Parmis la grande diversité de *générateur de site web statique*, tels que [Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io/), [Nikola](https://getnikola.com/), nous avons choisi d'experimenter [Quarto](https://quarto.org/) pour ses capacités à traiter une grande diversité de contenus scientifiques.

### Quarto en quelques mots

Il suffit de décrire les pages souhaités dans des fichiers textes (.QMD) en utilisant la syntaxe légère markdown, dans lesquels il est possible d'introduire des tronçons de code python (ou R ou observableJs) qui une fois *compilés* par Quarto peuvent produire des fichiers html.
 

Les fichiers QMD (Quarto MarkDown/markdown épicés) sont générés par du code Python qui interroge Nakala.

## Galerie de projets

- [Bibliothèque numérique des archives Eduardo Manet](https://mshs-poitiers.gitpages.huma-num.fr/crla/eduardo-manet/)
- [Bibliothèque numérique du fonds d’archives Didier Dacunha-Castelle](https://mshs-poitiers.gitpages.huma-num.fr/crla/dacunha-castelle)
- [Bibliothèque numérique du fonds d’archives Nivaria Tejera - Fayad Jamís](https://mshs-poitiers.gitpages.huma-num.fr/crla/nivaria-tejera-fayad-jamis/)




