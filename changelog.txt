17/10/2024
ntq_utils.py
Added a test for existence when controlling embargo end date: some old data don't have one !

nakala_to_qmd.py
Added a test for existence of file named collection_Template.stats.qmd.
If it don't exist, do no try to calculate statistics.

06/11/2024
Updated documentation and recompiled nakala_to_qmd.exe.
