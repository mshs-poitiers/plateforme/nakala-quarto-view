# -*- coding: utf-8 -*-
'''
file: nakala_to_qmd.py
This program creates QMD files for Quarto (https://quarto.org), from templates \
and results from requests against Nakala API (collections and data).

Created on Tue Oct 31 14:46:29 2023
Last updated on Thu oct 17 09:56:05 2024
@author: Michael Nauge & David Chesnet

'''

# pylint: disable=line-too-long



from pathlib import Path


# objects from library nakalapycon to interact with Nakala API
import NklTarget as nklt
import nklAPI_Datas as nkl_data
import nklAPI_Collections as nkl_collection

# our constants
import ntq_constants as const

# our helper functions
from ntq_utils import normalize_name, get_metas_by_property, is_collection_id, get_str_after_key, get_point_locations, \
    convert_dcmi_dcsv_point, isReadyToView, get_year

# API_URL = ""
# collections_id = []

def get_collections_id():
    '''
    Returns a list of collections Ids listed within a text file

    Parameters
    ----------
    thefile: String

    Returns
    -------
    coll_list: List
        collections Ids
    '''

    coll_list =[]
    with open("collections_ids.txt", "r",encoding="utf-8")as txtfile:
        lines = txtfile.readlines()
        for line in lines:
            a_coll_id = line.strip()

            if (len(a_coll_id) == 0) or (a_coll_id[0] == "#"):
                continue
            if is_collection_id(a_coll_id):
                coll_list.append(a_coll_id)
            else:
                print(a_coll_id + " is not a valid Nakala collection identifier")
    return coll_list


def get_datafile_description(thefile):
    '''
    Returns the description associated with a file

    Parameters
    ----------
    thefile: Dict

    Returns
    -------
    desc: String
        file description
    '''

    if ("description" in thefile) and not thefile["description"] is None:
        desc = thefile["description"]
    else:
        desc = ""
    return desc



def get_title(rec):
    '''
    Parameters
    ----------
    rec : Dictionnary
        DESCRIPTION.

    Returns
    -------
    a_title : String
        DESCRIPTION.
    '''
    a_title = get_metas_by_property(rec, const.NAKALATERM + 'title', '')[0].replace('"','\\"')
    return a_title


# Functions to process a collection



def get_data_collections_titles(nkl_target, collection_doi):
    '''
    Retrieves a collection title, given its DOI

    Parameters
    ----------
    nkl_target : Object
        Connector to a Nakala repository.
    collection_doi : String
        Contains the collection DOI.

    Returns
    -------
    collection_title : String
        The title of the collection
    '''

    res = nkl_collection.get_collections(nkl_target, collection_doi)
    if res.isSuccess is True:
        collection_title = get_title(res) # get_metas_by_property(res, const.NAKALATERM + 'title', '')[0].replace('"','\\"')
    return collection_title




def get_all_data_doi(nklt_target, data_doi_collection):
    '''
    Retrieves DOI of every data within a collection

    Parameters
    ----------
    nklt_target : Object
        Connector to a Nakala repository.
    data_doi_collection : String
        The DOI of a collection.

    Returns
    -------
    all_dois : List
        list of all data DOIs within the collection.
    '''

    page = 1
    all_dois = []

    while True:
        response = nkl_collection.get_collections_datas(nklt_target, data_doi_collection, page=page)

        if not response.isSuccess:
            print("Error retrieving collection " + data_doi_collection + " datas DOI")
            return all_dois

        data = response.dictVals['data']
        for item in data:
            a_doi = item.get('identifier')
            if a_doi:
                all_dois.append(a_doi)

        last_page = response.dictVals['lastPage']
        if page >= last_page:
            break
        page += 1

    return all_dois



def get_collection_data_stats(nkl_target, a_collection_doi):
    '''
    Collecte des informations sur la distribution des types de fichiers dans la collection

    Parameters
    ----------
    nkl_target : Object
        A connector to Nakala API.
    a_collection_doi : String
        The DOI of a collection.

    Returns
    -------
    typology : Dict
        Contains a collection number of data, number of files and
        the distribution of file content type (image, audio, etc.)
    '''

# application/json; application/pdf; text/plain; word;
    typology = {"image":0, "audio":0, "video":0, "pdf":0, "spreadsheet":0, "csv": 0, "text":0, "other":0}

    nb_files = 0
    nb_data =0

    datas_doi = get_all_data_doi(nkl_target, a_collection_doi)
    if len(datas_doi) < 1:
        return typology

    nb_data = len(datas_doi)
    for a_data_doi in datas_doi:
        rec = nkl_data.get_datas(nkl_target, a_data_doi)
        if rec.isSuccess is False:
            print("Error retrieving data " + a_data_doi)
            return typology

        data_files = rec.dictVals["files"]
        nb_files += len(data_files)
        # Identify every data type in a list
        for data_file in data_files:
            if (data_file["mime_type"].find("image")) != -1:
                if typology.get("image"):
                    typology["image"] += 1
                else:
                    typology["image"] = 1
            elif (data_file["mime_type"].find("audio")) != -1:
                if typology.get("audio"):
                    typology["audio"] += 1
                else:
                    typology["audio"] = 1
            elif (data_file["mime_type"].find("video")) != -1:
                if typology.get("video"):
                    typology["video"] += 1
                else:
                    typology["video"] = 1
            elif (data_file["mime_type"].find("pdf")) != -1:
                if typology.get("pdf"):
                    typology["pdf"] += 1
                else:
                    typology["pdf"] = 1
            elif (data_file["mime_type"].find("spreadsheet")) != -1:
                if typology.get("spreadsheet"):
                    typology["spreadsheet"] += 1
                else:
                    typology["spreadsheet"] = 1
            elif (data_file["mime_type"].find("csv")) != -1:
                if typology.get("csv"):
                    typology["csv"] += 1
                else:
                    typology["csv"] = 1
            elif (data_file["mime_type"].find("plain")) != -1:
                if typology.get("text"):
                    typology["text"] += 1
                else:
                    typology["text"] = 1
            else:
                if typology.get("other"):
                    typology["other"] += 1
                else:
                    typology["other"] = 1
    typology["nbData"] = nb_data
    typology["nbFiles"] = nb_files

    return typology



def gen_collection_stats_page(nkl_target, collection_doi, collec_doi_name, collection_path):
    '''
    Creates a page of basic statistics for a collection

    Parameters
    ----------
    nkl_target : Object
        Connector to a Nakala repository.
    collection_doi : String
        DOI of a collection in the Nakala repository.
    collec_doi_name : String
        DOI with "." and "/" replaced to be usable as a file or folder name.
    collection_path : String
        Folder location for the collection files.

    Returns
    -------
    None.
    '''

    output_text = ""

    rec = nkl_collection.get_collections(nkl_target, collection_doi)
    if rec.isSuccess is True:

        type_distribution = get_collection_data_stats(nkl_target, collection_doi)
        if len(type_distribution) > 0:

            with open(const.COLLECTION_STATS_TEMPLATE, "r", encoding = "utf-8") as cs_file:
                for line in cs_file:
                    if line.find("<CollectionTitle>") != -1:

                        # Escape quotes within the text
                        output_text += line.replace("<CollectionTitle>", get_title(rec)) # get_metas_by_property(rec, const.NAKALATERM + 'title', '')[0].replace('"','\\"'))

                    elif line.find("<xfiles>") != -1:
                        output_text += line.replace("<xfiles>", f'{type_distribution["nbFiles"]}').replace("<xdata>", \
                                                                   f'{type_distribution["nbData"]}')
                        del type_distribution["nbFiles"]
                        del type_distribution["nbData"]
                    elif line.find("<typeDistribution>") != -1:
                        # replaces the tag "<typeDistribution>" by the content of dictionnary type_distribution
                        data_export ={}
                        data_export['type'] = list(type_distribution.keys())
                        data_export['number']= list(type_distribution.values())
                        output_text += line.replace("<typeDistribution>", str(data_export))
                    else:
                        output_text += line

            # Writes the modified content in a new .qmd file
            modified_page_path = Path(collection_path + "/stats/", collec_doi_name + ".stats.qmd")
            modified_page_path.write_text(output_text, encoding = "utf-8")

            print(modified_page_path)




def gen_collection_page(nkl_target, collection_doi, collec_doi_name, collection_path):
    '''
    Creates a page to present a collection data

    Parameters
    ----------
    nkl_target : Object
        Connector to a Nakala repository.
    collection_doi : String
        DOI of a collection.
    collec_doi_name : String
        DOI with "." and "/" replaced to be usable as a file or folder name.
    collection_path : String
        Folder location for "collection" files.

    Returns
    -------
    None.
    '''

    rec = nkl_collection.get_collections(nkl_target, collection_doi)

    if rec.isSuccess is False:
        return

    output_text = ""

    with open(const.COLLECTION_TEMPLATE, "r", encoding = "utf-8") as coll_file:
        for line in coll_file:

            if line.find("<CollectionTitle>") != -1:
                output_text += line.replace("<CollectionTitle>", get_title(rec)) # get_metas_by_property(rec, const.NAKALATERM + 'title', '')[0].replace('"','\\"'))

            elif line.find("<CollDoiName>") != -1:
                output_text += line.replace("<CollDoiName>", collec_doi_name)

            elif line.find("<Description>") != -1 :
                desc = get_metas_by_property(rec, const.DCTERM + 'description', '')
                output_text += line.replace('<Description>', desc[0])

            elif line.find("Localisation") != -1:

                datas_doi = get_all_data_doi(nkl_target, collection_doi)
                dic_points_list = []
                for data_doi in datas_doi:
                    rcd = nkl_data.get_datas(nkl_target, data_doi)
                    data_url = const.SITE_DATA_PATH + normalize_name(collection_doi) + "/" + normalize_name(data_doi) + ".html"
                    if rcd.isSuccess is True:
                        points = get_point_locations(rcd)
                        if points[0] != "":
                            for point in points:
                                dic_point = convert_dcmi_dcsv_point(point)
                                dic_point["URL"] = ".." + data_url
                                dic_points_list.append(dic_point)

                if len(dic_points_list) > 0:
                    output_text += line
                    with open(const.MAP_TEMPLATE, "r", encoding = "utf-8") as map_temp:
                        for map_temp_line in map_temp:
                            if map_temp_line.find("<listePoints>") != -1:
                                output_text += map_temp_line.replace("<listePoints>", str(dic_points_list))
                            else:
                                output_text += map_temp_line
            else:
                output_text += line
        # Writes the modified content in a new .qmd file
        modified_page_path = Path(collection_path, collec_doi_name + '.qmd')
        modified_page_path.write_text(output_text, encoding = "utf-8")

        print(modified_page_path)



# Functions to process a data


def set_file_name(txt_line, the_file):
    '''
    Replaces the tag "fileName" with a file name within a text line.

    Parameters
    ----------
    txt_line : String
        A line of text where a file name should be inserted.
    the_file : Dictionnary
        Metadata associated with a data file.

    Returns
    -------
    new_line : String
        text line containing the "true" file name
    '''

    new_line = txt_line.replace("fileName", "`" + the_file["name"] + "`")
    return new_line



def process_line(line, a_doi, a_datafile, a_title, thumb, player_embed):
    '''
    Replaces fields in a line of text with specified values for a datafile
    Parameters
    ----------
    line : String
        Line of text read in the template file
    a_doi : String
        a DOI identifier
    a_datafile : Dictionnary
        contains fields describing files (name, etc.)
    a_title : String
        file description
    thumb : String
        path to an icon or thumbnail
    player_embed : Boolean
        An URL to a Nakala embedded player, or to download the file (may then be opened in the web browser)

    Returns
    -------
    new_line : String
        A line containing specific information for the processed file, to insert into the QMD file
    '''

    sha1 = a_datafile["sha1"]
    download_url = API_URL + "data/" +  a_doi + "/" + sha1

    if player_embed:
        player_url = API_URL + "embed/" +  a_doi + "/" + sha1
    else:
        player_url = download_url

    new_line = set_file_name(line, a_datafile).replace("  ", "  \n")\
    .replace(a_title, get_datafile_description(a_datafile))\
    .replace("ThumbnailURL", thumb)\
    .replace("PlayerURL", player_url)\
    .replace("URLDownload", download_url) + "\n"
    return new_line



def get_data_type_list(line, data_files):
    '''
    Makes a list of each datafile type, lists the differents types and adds icon information when there is only one type
    that is not an image (Quarto makes thumbnail for images)

    Parameters
    ----------
    line : TYPE
        DESCRIPTION.
    data_files : TYPE
        DESCRIPTION.

    Returns
    -------
    data_type_list : Array of dictionnary
        list the content type of each file in a Nakala data.
    add_on : String
        things to add in the resulting QMD file.

    '''
    type_array = []
    add_on = ""
    data_type_list =[]

    # Identify every file data type in a list
    for data_file in data_files:
        if (data_file["mime_type"].find("image"))!= -1:
            data_type_list.append("image")
        elif (data_file["mime_type"].find("audio"))!= -1:
            data_type_list.append("audio")
        elif (data_file["mime_type"].find("video"))!= -1:
            data_type_list.append("video")
        elif (data_file["mime_type"].find("pdf"))!= -1:
            data_type_list.append("pdf")
        elif (data_file["mime_type"].find("spreadsheet"))!= -1:
            data_type_list.append("spreadsheet")
        elif (data_file["mime_type"].find("csv"))!= -1:
            data_type_list.append("csv")
        elif (data_file["mime_type"].find("plain"))!= -1:                # mime typetext/plain
            data_type_list.append("text")
        else:
            print("mime type" + data_file["mime_type"])
            data_type_list.append("other")

        # Make a disctinct values list
        if not data_type_list[-1] in type_array:
            type_array.append(data_type_list[-1])

    # Set image type first (to get a nice tag order)

    if "image" in type_array:
        idx = type_array.index("image")
        type_array.pop(idx)
        type_array.insert(0, "image")


    # Declare every data type in YAML "header"
    for a_type in type_array:
        add_on += line.replace("TypeData", a_type)

    # If the data has a single data type, set a default image in the "header" part of the data QMD file
    if len(type_array) == 1:
        if (type_array[0].find("image"))!= -1:
            pass
        elif (type_array[0].find("audio"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "ondes.png\n"
        elif (type_array[0].find("video"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "video.png\n"
        elif (type_array[0].find("pdf"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "pdf.png\n"
        elif (type_array[0].find("text"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "txt.png\n"
        elif (type_array[0].find("spreadsheet"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "spreadsheet.png\n"
        elif (type_array[0].find("csv"))!= -1:
            add_on += 'image: ' + const.ICON_PATH + "csv.png\n"
        else:
            add_on += 'image: ' + const.ICON_PATH + "unknown.png\n"

    return data_type_list, add_on



def gen_data_page(nkl_target, data_doi, data_in_collection_path):
    '''
    Generates a page with all files contained within a data

    Parameters
    ----------
    nkl_target : Object
        Connector to a Nakala repository
    data_doi: String
        DOI a of specific data
    data_in_collection_path: String

    Returns
    -------
    None.
    '''

    rec = nkl_data.get_datas(nkl_target, data_doi)

    if rec.isSuccess is False:
        print("Error retrieving data " + data_doi)
        return

    output_text = ""
    data_files = rec.dictVals["files"]
    # print(rec)
    with open(const.DATA_TEMPLATE, "r", encoding = "utf-8") as dt_file:

        for line in dt_file:
            if line.startswith('[comment]:'):
                continue

            if line.find("LeTitre") != -1:
                output_text += line.replace("LeTitre", get_title(rec))

            # Process data files types
            elif line.find("TypeData") != -1:
                data_type_list, add_on = get_data_type_list(line, data_files)
                output_text += add_on

            elif line.find("LEDOI") != -1:
                 output_text += line.replace("LEDOI", data_doi)

            elif line.find("NomDeLauteur") != -1:
                les_auteurs = ""
                autors = get_metas_by_property(rec, const.NAKALATERM + 'creator', '')
                for i, autor in enumerate(autors):
                    if autor != "":
                        les_auteurs +=  autor['givenname'] + ", " +autor['surname'] + "  \n"   # two white spaces are required in Markdown for a line jump
                    else:
                        les_auteurs +=  const.UNKNOWN + "  \n"
                if les_auteurs != "":
                    output_text += line.replace("NomDeLauteur", "\n" + les_auteurs)

            elif line.find("NomContributeur") != -1:
                contributors = get_metas_by_property(rec, const.DCTERM + 'contributor', '')
                les_contrib = ""
                for i, contrib in enumerate(contributors):
                    if contrib != "":
                        les_contrib += contrib + "  \n"  # two white spaces are required in Markdown for a line jump
                if les_contrib != "":
                    output_text += line.replace("NomContributeur", "\n" + les_contrib)

            # Our our own Citation, not that of Nakala
            elif line.find("LaCitation") != -1:
                les_auteurs = ""
                # print(get_metas_by_property(rec, const.NAKALATERM + 'creator', ''))
                autors = get_metas_by_property(rec, const.NAKALATERM + 'creator', '')
                #☺ print(autors)
                if autors != [""]:
                    if autors[0]['givenname'] != const.UNKNOWN:
                        for i, autor in enumerate(autors):
                            if i > 0:
                                les_auteurs += '; '
                            les_auteurs += autor['givenname'] + ', ' + autor['surname']
                else:
                    autors = get_metas_by_property(rec, const.DCTERM + 'creator', '')
                    for i, autor in enumerate(autors):
                        if autor != '':
                            if i > 0:
                                les_auteurs += '; '
                            les_auteurs += autor
                ladate = get_metas_by_property(rec, const.NAKALATERM + 'created', '')
                if ladate == ['']:
                    ladate = get_metas_by_property(rec, const.DCTERM + 'created', '')
                    if ladate == ['']:
                        ladate[0] = const.NODATE
                les_auteurs += ' (' + get_year(ladate[0]) + '). '
                les_auteurs += get_title(rec) + '. '
                les_auteurs += '[' + const.resource_type.get(get_metas_by_property(rec, const.NAKALATERM + 'type', '')[0]) + ']. '
                output_text += line.replace("LaCitation", les_auteurs + 'NAKALA. https://doi.org/' + data_doi)

            elif line.find("CollectionId") != -1:
                for i in range(len(rec.dictVals["collectionsIds"])):

                    coll_link = '<a href="https://nakala.fr/collection/' + rec.dictVals["collectionsIds"][i] + '">' + rec.dictVals["collectionsIds"][i] + '</a>'
                    coll_title = get_data_collections_titles(nkl_target, rec.dictVals["collectionsIds"][i])
                    output_text += line.replace("CollectionId", coll_link + " [ **" + coll_title + "** ]")

            # Process specific metadata, specifically named within the template
            elif line.find(":META:") != -1:
                str_result = get_str_after_key(":META:", line)
                metas_terms_val = const.metas_terms.get(str_result)
                # print(metas_terms_val[0], " ", metas_terms_val[1], '\n')
                if metas_terms_val is not None:
                    metas_values = get_metas_by_property(rec, metas_terms_val[0], '')

                    the_values = ""
                    for i, metas_value in enumerate(metas_values):
                        if metas_value != "":
                            if metas_terms_val[0] == (const.NAKALATERM + "type"):
                                res_type = const.resource_type.get(metas_value)
                                the_values +=  res_type + "  \n"
                            else:
                                the_values +=  metas_value.replace("\n", "  \n") + "  \n"
                    if the_values != "":
                        output_text += "**" + metas_terms_val[1] + "**: "
                        output_text += line.replace(":META:" + str_result, the_values)


            # Process data files by content type (can be multi-valued)
            elif line.find("Localisation") != -1:
                points = get_point_locations(rec)

                if points[0] != "":
                    output_text += line
                    dic_points_list = []
                    for point in points:
                        dic_point = convert_dcmi_dcsv_point(point)
                        dic_points_list.append(dic_point)
                    with open(const.MAP_TEMPLATE, "r", encoding = "utf-8") as map_temp:
                        for map_temp_line in map_temp:
                            if map_temp_line.find("<listePoints>") != -1:
                                output_text += map_temp_line.replace("<listePoints>", str(dic_points_list))
                            else:
                                output_text += map_temp_line

            elif line.find('TitreImage') != -1:
                for i, adatafile in enumerate(data_files): # plantage avec 10.34847/nkl.9ebe386c
                    if data_type_list[i] == "image":
                        is_visible = isReadyToView(adatafile["embargoed"])    # "embargoed": "2024-03-26T00:00:00+01:00"
                        if is_visible:
                            output_text += process_line(line, data_doi, adatafile, "TitreImage",\
                                           API_URL + "iiif/" + data_doi + "/" + adatafile["sha1"] + "/full/!200,200/0/default.jpg", True)
                        else:
                            output_text += process_line(line, data_doi, adatafile, "TitreImage",const.ICON_PATH + "embargoed.png", True)

            elif line.find('TitreVideo') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "video":
                        output_text += process_line(line, data_doi, adatafile, "TitreVideo", const.ICON_PATH + "video.png", True)

            elif line.find('TitrePdf') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "pdf":
                        output_text += process_line(line, data_doi, adatafile, "TitrePdf", const.ICON_PATH + "pdf.png", True)

            elif line.find('TitreTexte') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "text":
                        output_text += process_line(line, data_doi, adatafile, "TitreTexte", const.ICON_PATH + "txt.png", True)

            elif line.find('TitreTableur') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "spreadsheet":
                        output_text += process_line(line, data_doi, adatafile, "TitreTableur", const.ICON_PATH + "spreadsheet.png", True)

                    if data_type_list[i] == "csv":
                        output_text += process_line(line, data_doi, adatafile, "TitreTableur", const.ICON_PATH + "csv.png", True)

            elif line.find('<div class="audio">') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "audio":
                        output_text += process_line(line.replace("audio/xxx", data_files[i]['mime_type']),\
                                                        data_doi, adatafile, "TitreSon", "", False)

            # For other types, give a link to the file with a default icon
            elif line.find('TitreUnknown') != -1:
                for i, adatafile in enumerate(data_files):
                    if data_type_list[i] == "other":
                        output_text += process_line(line, data_doi, adatafile, "TitreUnknown", const.ICON_PATH + "unknown.png", False)


            elif line.find('<div class="footer">') != -1:
                la_date = rec.dictVals["creDate"].split("T")[0]
                output_text += line.replace("TheDepositor", rec.dictVals["depositor"]["name"])\
                                   .replace("DepositDate", la_date)\
                                   .replace("TheLicence", get_metas_by_property(rec, const.NAKALATERM +'license',"")[0])

            else:
                output_text += line

            # Writes the modified content in a new .qmd file
        nom_fich = normalize_name(data_doi)
        modified_page_path = Path(data_in_collection_path, nom_fich + '.qmd')
        modified_page_path.write_text(output_text, encoding = "utf-8")

        print(modified_page_path)





# Main program
def main():
    '''

    Returns
    -------
    None.

    '''

    # If missing, create the folders for collections, collections/stats and data
    Path(const.DATA_PATH).mkdir(parents = True, exist_ok = True)
    Path(const.COLLECTIONS_PATH).mkdir(parents = True, exist_ok = True)
    Path(const.COLLECTIONS_PATH + "/stats/").mkdir(parents = True, exist_ok = True)

    collections_id = get_collections_id()
    # print(collections_id)

    # Process each collection DOI in the list
    for collec_id in collections_id:

        collect_id_name = normalize_name(collec_id)

        # generate a page to present the collection ans its data
        gen_collection_page(nklT_prod, collec_id, collect_id_name, const.COLLECTIONS_PATH)

        # generate a statistics page for the collection, if the template exists
        if Path(const.COLLECTION_STATS_TEMPLATE).exists():
            gen_collection_stats_page(nklT_prod, collec_id, collect_id_name, const.COLLECTIONS_PATH)

        # If missing, create a folder to store the collection data pages
        the_data_path = const.DATA_PATH + collect_id_name
        Path(the_data_path).mkdir(parents = True, exist_ok = True)

        # Retrieve the list of data DOI within the collection
        datas_dois = get_all_data_doi(nklT_prod, collec_id)

        # generate a page for each data
        for doi in datas_dois:
            gen_data_page(nklT_prod, doi, the_data_path)



if __name__ == '__main__':
    # Setup a connection to Nakala (read only, not need to write)
    nklT_prod = nklt.NklTarget(isNakalaProd = True, apiKey = "")
    API_URL = nklT_prod.API_URL + "/"                         # should be "https://api.nakala.fr/"
    main()
