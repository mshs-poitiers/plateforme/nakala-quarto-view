# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 14:46:29 2023

@author: dchesnet
"""

NAKALATERM = "http://nakala.fr/terms#"                          # nkl_target.BASE_URL + "/"
DCTERM = "http://purl.org/dc/terms/"

nakala_collection_root = "10.34847/nkl."

# path to store produced QMD files
SITE_ROOT = '../env_quarto'
COLLECTIONS_PATH = SITE_ROOT + '/collectionPost/'
SITE_DATA_PATH = '/dataPost/'
DATA_PATH = SITE_ROOT + SITE_DATA_PATH
ICON_PATH = "../../imgs/"


# path to templates files (used to build QMD files)
COLLECTION_TEMPLATE = "./collectionTemplate.qmd"
COLLECTION_STATS_TEMPLATE = './collectionTemplate.stats.qmd'
DATA_TEMPLATE = "./dataTemplate.qmd"
MAP_TEMPLATE = "./mapTemplate.qmd"

UNKNOWN = "Anonyme"
NODATE = "Inconnue"

# list of metadata supported by Nakala, that we can interpret/replace
# structure of an entry in the list is: { Identifier: [URL, Description] }
metas_terms = {
"nakalaterms:title" : ["http://nakala.fr/terms#title", "Titre"],
"nakalaterms:creator" : ["http://nakala.fr/terms#creator", "Créateur"],
"nakalaterms:created" : ["http://nakala.fr/terms#created", "Date de création"],
"nakalaterms:license" : ["http://nakala.fr/terms#license", "Licence"],
"nakalaterms:type" : ["http://nakala.fr/terms#type", "Type"],
"dcterms:title" : ["http://purl.org/dc/terms/title", "Titre"],
"dcterms:creator" : ["http://purl.org/dc/terms/creator", "Créateur"],
"dcterms:subject" : ["http://purl.org/dc/terms/subject", "Sujet"],
"dcterms:description" : ["http://purl.org/dc/terms/description", "Description"],
"dcterms:publisher" : ["http://purl.org/dc/terms/publisher" , "Editeur"],
"dcterms:contributor" : ["http://purl.org/dc/terms/contributor", "Contributeur"],
"dcterms:date" : ["http://purl.org/dc/terms/date", "Date"],
"dcterms:type" : ["http://purl.org/dc/terms/type", "Type"],
"dcterms:format" : ["http://purl.org/dc/terms/format", "Format"],
"dcterms:identifier" : ["http://purl.org/dc/terms/identifier", "Identifiant"],
"dcterms:source" : ["http://purl.org/dc/terms/source", "Source"],
"dcterms:language" : ["http://purl.org/dc/terms/language", "Langue"],
"dcterms:relation" : ["http://purl.org/dc/terms/relation", "Relation"],
"dcterms:coverage" : ["http://purl.org/dc/terms/coverage", "Couverture"],
"dcterms:rights" : ["http://purl.org/dc/terms/rights", "Droits"],
"dcterms:audience" : ["http://purl.org/dc/terms/audience", "Audience"],
"dcterms:alternative" : ["http://purl.org/dc/terms/alternative", "Alternative"],
"dcterms:tableOfContents" : ["http://purl.org/dc/terms/tableOfContents", "Sommaire"],
"dcterms:abstract" : ["http://purl.org/dc/terms/abstract", "Résumé"],
"dcterms:created" : ["http://purl.org/dc/terms/created", "Créé"],
"dcterms:valid" : ["http://purl.org/dc/terms/valid", "Valide"],
"dcterms:available" : ["http://purl.org/dc/terms/available", "Disponible"],
"dcterms:issued" : ["http://purl.org/dc/terms/issued", "Emis"],
"dcterms:modified" : ["http://purl.org/dc/terms/modified", "Modifié"],
"dcterms:extent" : ["http://purl.org/dc/terms/extent", "Etendue"],
"dcterms:medium" : ["http://purl.org/dc/terms/medium" , "Support"],
"dcterms:isVersionOf" : ["http://purl.org/dc/terms/isVersionOf", "Est version de"],
"dcterms:hasVersion" : ["http://purl.org/dc/terms/hasVersion", "A la version"],
"dcterms:isReplacedBy" : ["http://purl.org/dc/terms/isReplacedBy", "Est remplacé par"],
"dcterms:replaces" : ["http://purl.org/dc/terms/replaces", "Remplace"],
"dcterms:isRequiredBy" : ["http://purl.org/dc/terms/isRequiredBy", "Est requis par"],
"dcterms:requires" : ["http://purl.org/dc/terms/requires", "Requiert"],
"dcterms:isPartOf" : ["http://purl.org/dc/terms/isPartOf", "Fait partie de"],
"dcterms:hasPart" : ["http://purl.org/dc/terms/hasPart", "Contient des parties"],
"dcterms:isReferencedBy" : ["http://purl.org/dc/terms/isReferencedBy", "Est référencé par"],
"dcterms:references" : ["http://purl.org/dc/terms/references", "Référence"],
"dcterms:isFormatOf" : ["http://purl.org/dc/terms/isFormatOf", "Est le format de"],
"dcterms:hasFormat" : ["http://purl.org/dc/terms/hasFormat", "A le format"],
"dcterms:conformsTo" : ["http://purl.org/dc/terms/conformsTo", "Se conforme à"],
"dcterms:spatial" : ["http://purl.org/dc/terms/spatial" , "Spatial"],
"dcterms:temporal" : ["http://purl.org/dc/terms/temporal", "Temporel"],
"dcterms:mediator" : ["http://purl.org/dc/terms/mediator", "Médiateur"],
"dcterms:dateAccepted" : ["http://purl.org/dc/terms/dateAccepted", "Accepté le"],
"dcterms:dateCopyrighted" : ["http://purl.org/dc/terms/dateCopyrighted", "Date copyright"],
"dcterms:dateSubmitted" : ["http://purl.org/dc/terms/dateSubmitted", "Soumis le"],
"dcterms:educationLevel" : ["http://purl.org/dc/terms/educationLevel", "niveau d'éducation"],
"dcterms:accessRights" : ["http://purl.org/dc/terms/accessRights", "Droits d'accès"],
"dcterms:bibliographicCitation" : ["http://purl.org/dc/terms/bibliographicCitation", "Citation bilbiographique"],
"dcterms:license" : ["http://purl.org/dc/terms/license", "Licence"],
"dcterms:rightsHolder" : ["http://purl.org/dc/terms/rightsHolder", "Propriétaire des droits"],
"dcterms:provenance" : ["http://purl.org/dc/terms/provenance", "Provenance"],
"dcterms:instructionalMethod" : ["http://purl.org/dc/terms/instructionalMethod", "méthode pédagogique"],
"dcterms:accrualMethod" : ["http://purl.org/dc/terms/accrualMethod" , "méthode de cumul"],
"dcterms:accrualPeriodicity" : ["http://purl.org/dc/terms/accrualPeriodicity" , "Périodicité de cumul"],
"dcterms:accrualPolicy" : ["http://purl.org/dc/terms/accrualPolicy" , "Politique de cumul"]
}


# Closed list of resource type allowed in Nakala

resource_type = {
"http://purl.org/coar/resource_type/c_c513" : "image",
"http://purl.org/coar/resource_type/c_12ce" : "video",
"http://purl.org/coar/resource_type/c_18cc" : "son",
"http://purl.org/coar/resource_type/c_6501" : "Article de journal",
"http://purl.org/coar/resource_type/c_6670" : "poster",
"http://purl.org/coar/resource_type/c_c94f" : "présentation",
"http://purl.org/coar/resource_type/c_e059" : "cours",
"http://purl.org/coar/resource_type/c_2f33" : "livre",
"http://purl.org/coar/resource_type/c_12cd" : "carte",
"http://purl.org/coar/resource_type/c_ddb1" : "dataset",
"http://purl.org/coar/resource_type/c_5ce6" : "logiciel",
"http://purl.org/coar/resource_type/c_1843" : "autres",
"http://purl.org/library/ArchiveMaterial" : "fonds d’archives",
"http://purl.org/ontology/bibo/Collection" : "exposition d’art",
"http://purl.org/coar/resource_type/c_86bc" : "bibliographie",
"http://purl.org/ontology/bibo/Series" : "bulletin",
"http://purl.org/coar/resource_type/c_ba08" : "édition de sources",
"http://purl.org/coar/resource_type/c_0040" : "manuscrit",
"http://purl.org/coar/resource_type/c_0857" : "correspondance",
"http://purl.org/coar/resource_type/c_93fc" : "rapport",
"http://purl.org/coar/resource_type/c_2659" : "périodique",
"http://purl.org/coar/resource_type/c_816b" : "prépublication",
"http://purl.org/coar/resource_type/c_efa0" : "recension",
"http://purl.org/coar/resource_type/c_18cw" : "partition",
"https://w3id.org/survey-ontology#SurveyDataSet" : "données d’enquêtes",
"http://purl.org/coar/resource_type/c_18cf" : "texte",
"http://purl.org/coar/resource_type/c_46ec" : "thèse",
"http://purl.org/coar/resource_type/c_7ad9" : "page web",
"http://purl.org/coar/resource_type/c_beb9" : "data paper",
"http://purl.org/coar/resource_type/c_e9a0" : "article programmable"
}

