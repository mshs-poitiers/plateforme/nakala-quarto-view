# -*- coding: utf-8 -*-
"""
Utility module for nakala_to_qmd.py
Created on Mon Apr 29 14:54:00 2024

@author: dchesnet
"""

import re
import datetime as dt
# our constants
import ntq_constants as const


def normalize_name(name):
    '''
    Modifies a name such that it can be used as a file/directory name

    Parameters
    ----------
    name : String

    Returns
    -------
    String

    '''
    return name.replace(".", "_").replace("/", "-")


# Functions to retrieve metadata values
# not used yet: get_metas_by_type, get_val_from_label

def get_metas_by_type(rec, type_uri):
    '''
    Returns a list of values associated with a given URI type
    Parameters
    ----------
    rc : Object

    type_URI : String

    Returns
    -------
    val : List of string
    '''

    data_metas = rec.dictVals["metas"]
    val = []

    for i in range(len(data_metas)):

        if data_metas[i]["typeUri"] == type_uri:
            if data_metas[i]["value"] is not None:
                val.append(data_metas[i]["value"])
    if not val:
        val.append("")
    return val



def get_metas_by_property(rec, prop, field):
    '''
    Returns a list of values associated with a given property (eventually specified within a subfield)
    Parameters
    ----------
    prop : String
        PropertyURI
    field : String
        subfield we want the value.

    Returns
    -------
    val: List of String.
    '''

    data_metas = rec.dictVals["metas"]
    val = []

    for i in range(len(data_metas)):
        if data_metas[i]["propertyUri"] == prop:
            if data_metas[i]["value"] is not None:
                if field != "":
                    val.append(data_metas[i]["value"][field])
                else:
                    val.append(data_metas[i]["value"])
    if not val:
        val.append("")
    return val



def is_collection_id(a_string):
    #% r"^(10.34847/nkl.)[a-z0-9]{8}"
    val = ""
    motif = rf"^({const.nakala_collection_root})[a-z0-9]{{8}}" # syntax in Python >= 3.7
    match = re.search(motif, a_string)
    if match:
        val = match.group(1)
    return val


def get_year(str):
    '''
    Parameters
    ----------
    str : String
        DESCRIPTION.

    Returns
    -------
    date : String
        Year on 4 digits, or "Unknown" constant
    '''
    date = re.match("\d{4}", str)
    if date:
        year = date.group(0)
    else:
        year = const.NODATE
    return year


# Functions to retrieve values associated with keywords (@Michael Nauge)

def get_str_after_key(key, input_str):
    '''
    Returns the remainder of a string, after a given "key"

    Parameters
    ----------
    key : String

    inputStr : String
        The string to process.

    Returns
    -------
    val : String
    '''

    val = ""
    # looking for the key, followed by any alphanum char or colon (can be more than once)
    motif = f"{key}" + r"([\w\:]+)"

    match = re.search(motif, input_str)
    if match:
        val = match.group(1)

    return val



def get_val_from_label(label, input_str):
    '''
    Returns the content associated with a "keyword" string

    Parameters
    ----------
    label : String

    inputStr : String
        The string to process.

    Returns
    -------
    val : String


    '''
    val = ""
    # Search for a specfific label followed by an equal sign (it is a regex language reserved sign, so we "escape" it),
    # then by caracters among -, letters, numbers, spaces, finally ended by a semi-colon (or none for the last label)
    motif = f"{label}" + r"\=([-\w\d\.\s]+);*"

    match = re.search(motif, input_str)
    if match:
        val = match.group(1)

    return val


# Functions to process location as a point

def convert_dcmi_dcsv_point(input_str):
    '''
    Converts a string containing information encoded with dcmi syntax into a dictionnary.
    See https://www.dublincore.org/specifications/dublin-core/dcmi-point/
    Default values are defined;
    See also https://documentation.huma-num.fr/nakala-release-notes/#2-mars-2023-nakala_press-v101

    Parameters
    ----------
    inputStr : String
        String containing spatial location information.

    Returns
    -------
    dic_point : Dictionnary

    '''

    dic_point = {}
    dic_point['east'] = get_val_from_label("east", input_str)
    dic_point['north'] = get_val_from_label("north", input_str)
    dic_point['elevation'] = get_val_from_label("elevation", input_str)
    dic_point['name'] = get_val_from_label("name", input_str)
    dic_point['units'] = "signed decimal degrees"
    dic_point['zunits'] = "metres"
    dic_point['projection'] = "WGS84"

    return dic_point


# Functions to retrieve specfic information within a data metadata or files

def get_point_locations(rec):
    '''
    Returns a list of location points values
    Parameters
    ----------
    rc : Object

    Returns
    -------
    val : List of dictionnary
    '''

    data_metas = rec.dictVals["metas"]
    val = []

    for i in range(len(data_metas)):
        if data_metas[i]["propertyUri"] == "http://purl.org/dc/terms/spatial":
            if data_metas[i]["typeUri"] == "http://purl.org/dc/terms/Point":
                if data_metas[i]["value"] is not None:
                    val.append(data_metas[i]["value"])
    if not val:
        val.append("")
    return val


# Function to process a date and say if it is passed

def isReadyToView(dateStr):
    # Ex: 2024-03-26T00:00:00+01:00
    theDate = dt.datetime.strptime(dateStr[0:10], "%Y-%m-%d")

    yet = dt.datetime.now()

    return not(theDate > yet)
